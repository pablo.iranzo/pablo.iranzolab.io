---
author: Pablo Iranzo Gómez
title: Some Lego vintage minifigures
tags: lego, minifigure vintage, pirates, space, fireman, ghost, doctor, tribes, archer
layout: post
date: 2020-02-22 20:30:24 +0100
comments: true
category: blog
description:
lang: en
---

Just some pics I made during some sorting of minifigs from my childhood.

<div class="elegant-gallery" itemscope itemtype="http://schema.org/ImageGallery">
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/CeV61xr.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/CeV61xr.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/pfYEfGJ.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/pfYEfGJ.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/4X7sNU8.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/4X7sNU8.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 </div>

Hope you like it!

Follow my channel on Telegram on Lego deals at <https://t.me/brickchollo>
