---
author: Pablo Iranzo Gómez
title: Lego 10242 - Mini Cooper Lego Creator
tags: lego, mini, lego 10242, review
layout: post
date: 2019-07-27 14:39:14 +0200
comments: true
category: blog
description:
---

I've uploaded those pics from the model, as it seems that people likes it.

The model is [Lego 10242 Mini Cooper 🛒](https://www.amazon.es/dp/B00M0ETSWU?tag=redken-21)
and the pics are for the standard instructions build.

<div class="elegant-gallery" itemscope itemtype="http://schema.org/ImageGallery">
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/ewbaE5b.jpg.jpg" itemprop="contentUrl" data-size="5312x2988">
            <img src="https://i.imgur.com/ewbaE5b.jpg" width="531" height="298" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/ndvDhi4.jpg.jpg" itemprop="contentUrl" data-size="2984x5312">
            <img src="https://i.imgur.com/ndvDhi4.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/ocHIcX3.jpg.jpg" itemprop="contentUrl" data-size="2984x5312">
            <img src="https://i.imgur.com/ocHIcX3.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/nkJ37xk.jpg.jpg" itemprop="contentUrl" data-size="2984x5312">
            <img src="https://i.imgur.com/nkJ37xk.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/RN3zp86.jpg.jpg" itemprop="contentUrl" data-size="2984x5312">
            <img src="https://i.imgur.com/RN3zp86.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/WZ5VIk8.jpg.jpg" itemprop="contentUrl" data-size="2984x5312">
            <img src="https://i.imgur.com/WZ5VIk8.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/s0t0PPb.jpg.jpg" itemprop="contentUrl" data-size="2984x5312">
            <img src="https://i.imgur.com/s0t0PPb.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/XS8MGtv.jpg.jpg" itemprop="contentUrl" data-size="2984x5312">
            <img src="https://i.imgur.com/XS8MGtv.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
</div>

Hope you like it!

Follow my channel on Telegram on Lego deals at <https://t.me/brickchollo>
