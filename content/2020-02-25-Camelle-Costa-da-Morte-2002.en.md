---
author: Pablo Iranzo Gómez
title: Camelle - Costa da Morte 2002
tags: petrol, spill, prestige
layout: post
date: 2020-02-25 21:30:24 +0100
comments: true
category: blog
description:
lang: en
---

Those are some pics I took during December in 2002 around Camelle and Costa da Morte area in North of Spain after the Prestige oil spill.

<div class="elegant-gallery" itemscope itemtype="http://schema.org/ImageGallery">
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/WpXrXUF.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/WpXrXUF.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/CkKAAQv.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/CkKAAQv.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/x8mgooY.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/x8mgooY.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/ZCGqEIk.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/ZCGqEIk.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/z5A2NHD.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/z5A2NHD.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/YpB4CPV.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/YpB4CPV.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/Rbex877.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/Rbex877.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/Q9ck1fR.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/Q9ck1fR.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/Qiat3F8.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/Qiat3F8.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/HJpjYMz.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/HJpjYMz.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/XT6aEBZ.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/XT6aEBZ.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/pHpeouI.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/pHpeouI.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/nautu1r.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/nautu1r.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/9qh0sXk.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/9qh0sXk.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/YztdWjU.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/YztdWjU.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/fOfiPQ3.jpg.jpg" itemprop="contentUrl" data-size="768x1024">
            <img src="https://i.imgur.com/fOfiPQ3.jpg" width="150" height="204" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/aF8j6OR.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/aF8j6OR.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/IkcQ70F.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/IkcQ70F.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/GkwY1be.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/GkwY1be.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/AXbSkIh.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/AXbSkIh.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/zshBJdg.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/zshBJdg.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/WBTA2yA.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/WBTA2yA.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/t5ATjuv.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/t5ATjuv.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/G2k4NrB.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/G2k4NrB.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/MjQI4ga.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/MjQI4ga.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/rAu1TW3.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/rAu1TW3.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/fcvxI4k.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/fcvxI4k.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/E3rzZrP.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/E3rzZrP.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/whXjW0P.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/whXjW0P.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/R1iMY3K.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/R1iMY3K.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/0VyhRzE.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/0VyhRzE.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/87YaYR4.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/87YaYR4.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/rWGEonO.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/rWGEonO.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/8AOrgdg.jpg.jpg" itemprop="contentUrl" data-size="1024x768">
            <img src="https://i.imgur.com/8AOrgdg.jpg" width="204" height="150" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/XwjQArW.jpg.jpg" itemprop="contentUrl" data-size="768x1024">
            <img src="https://i.imgur.com/XwjQArW.jpg" width="150" height="204" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/NOeptEJ.jpg.jpg" itemprop="contentUrl" data-size="768x1024">
            <img src="https://i.imgur.com/NOeptEJ.jpg" width="150" height="204" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/LlYEUqh.jpg.jpg" itemprop="contentUrl" data-size="768x1024">
            <img src="https://i.imgur.com/LlYEUqh.jpg" width="150" height="204" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/mrO6MOd.jpg.jpg" itemprop="contentUrl" data-size="768x1024">
            <img src="https://i.imgur.com/mrO6MOd.jpg" width="150" height="204" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/imAxGH1.jpg.jpg" itemprop="contentUrl" data-size="768x1024">
            <img src="https://i.imgur.com/imAxGH1.jpg" width="150" height="204" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/nToZQ8W.jpg.jpg" itemprop="contentUrl" data-size="768x1024">
            <img src="https://i.imgur.com/nToZQ8W.jpg" width="150" height="204" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
</div>
