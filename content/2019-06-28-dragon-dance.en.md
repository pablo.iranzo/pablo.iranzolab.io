---
author: Pablo Iranzo Gómez
title: Lego 80102 - Dragon dance
tags: Lego, Chinese, dragon dance, new year, review, lego 80102
layout: post
date: 2019-06-28 20:32:14 +0200
comments: true
category: blog
description: Lego Chinese Dragon Dance review
---

This is the pictures of the set that my colleague Raúl brought back from his trip to APAC.

The set, includes a mechanism that makes the dancers to move up and down and the lantern hold by the pig to rotate.

Model is [80102 Dragon Dance 🛒](https://www.amazon.es/dp/B07KRJJFY8?tag=redken-21)

<div class="elegant-gallery" itemscope itemtype="http://schema.org/ImageGallery">
    <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/8j1Drm8.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/8j1Drm8.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
    <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
    <a href="https://i.imgur.com/y72cWUS.jpg" itemprop="contentUrl" data-size="4032x3024">
        <img src="https://i.imgur.com/y72cWUS.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
    </a>
    <figcaption itemprop="caption description"></figcaption>
    </figure>
    <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
    <a href="https://i.imgur.com/EOD2WXW.jpg" itemprop="contentUrl" data-size="4032x3024">
        <img src="https://i.imgur.com/EOD2WXW.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
    </a>
    <figcaption itemprop="caption description"></figcaption>
    </figure>
        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
    <a href="https://i.imgur.com/XCjG4TR.jpg" itemprop="contentUrl" data-size="4032x3024">
        <img src="https://i.imgur.com/XCjG4TR.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
    </a>
    <figcaption itemprop="caption description"></figcaption>
    </figure>
        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
    <a href="https://i.imgur.com/sY9a1Qd.jpg" itemprop="contentUrl" data-size="4032x3024">
        <img src="https://i.imgur.com/sY9a1Qd.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
    </a>
    <figcaption itemprop="caption description"></figcaption>
    </figure>
</div>

Follow my channel on Telegram on Lego deals at <https://t.me/brickchollo>
