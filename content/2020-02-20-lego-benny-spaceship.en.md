---
author: Pablo Iranzo Gómez
title: Lego Benny Spaceship 70821
tags: lego, review, 70821, lego 70821, lego movie, benny, spaceship
layout: post
date: 2020-02-20 19:30:24 +0100
comments: true
category: blog
description:
lang: en
---

I bought [Lego 70821 Emmet Garage and Benny Spaceship 🛒](https://www.amazon.es/dp/B07FP2KS4F?tag=redken-21) because I loved the astronaut minifigures from my childhood, and I even loved the `broken` helmet (as I used to have them broken in the same way).

In this case, the spacecraft is an `adapted` version of the typical spaceship at that time, the `497-Galaxy Explorer`.

<div class="elegant-gallery" itemscope itemtype="http://schema.org/ImageGallery">
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/69ePXLB.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/69ePXLB.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/3iMki6z.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/3iMki6z.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/nov158s.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/nov158s.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/gXfNh1I.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/gXfNh1I.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/9juBiAV.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/9juBiAV.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
</div>

Hope you like it!

Follow my channel on Telegram on Lego deals at <https://t.me/brickchollo>
