---
author: Pablo Iranzo Gómez
title: Lego Fisherman Store 21310
tags: lego, fisherman, 21310, review
layout: post
date: 2020-02-23 23:30:24 +0100
comments: true
category: blog
description:
lang: en
---

Some pics about the 'almost' modular [Fisherman Store 🛒](https://www.amazon.es/dp/B06X9QM15K?tag=redken-21)

<div class="elegant-gallery" itemscope itemtype="http://schema.org/ImageGallery">
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/Y2sqxOq.jpg.jpg" itemprop="contentUrl" data-size="4032x1960">
            <img src="https://i.imgur.com/Y2sqxOq.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/JTF5Zax.jpg.jpg" itemprop="contentUrl" data-size="4032x1960">
            <img src="https://i.imgur.com/JTF5Zax.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/n0HIC2u.jpg.jpg" itemprop="contentUrl" data-size="4032x1960">
            <img src="https://i.imgur.com/n0HIC2u.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/JPUXy2M.jpg.jpg" itemprop="contentUrl" data-size="4032x1960">
            <img src="https://i.imgur.com/JPUXy2M.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 </div>

Hope you like it!

Follow my channel on Telegram on Lego deals at <https://t.me/brickchollo>
