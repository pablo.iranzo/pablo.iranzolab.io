---
author: Pablo Iranzo Gómez
title: Lego Speed Champions 76899 Lamborghini Urus ST-X & Lamborghini Huracán Super Trofeo EVO
tags: lego, review, Lamborghini, Speed champions, 76899
layout: post
date: 2020-04-30 22:30:24 +0200
comments: true
category: blog
description:
lang: en
---

Today I built the [Lamborghini Urus ST-X & Lamborghini Huracán Super Trofeo EVO 🛒](https://www.amazon.es/dp/B07W6Q9G1Y?tag=redken-21).

I liked specially the Huracán and the Urus was also very well done, a lot of details!

<div class="elegant-gallery" itemscope itemtype="http://schema.org/ImageGallery">
<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/dMor15o.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/dMor15o.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/WFZExx3.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/WFZExx3.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/wC3QWDb.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/wC3QWDb.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/f0RYTaH.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/f0RYTaH.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/gveoUgu.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/gveoUgu.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/R3old3o.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/R3old3o.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/91uRNPA.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/91uRNPA.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/mCbBcsD.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/mCbBcsD.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
</div>

Hope you like it!

Follow my channel on Telegram on Lego deals at <https://t.me/brickchollo>
