---
author: Pablo Iranzo Gómez
title: Lego 80101 - Chinese New Year's Eve
tags: lego, Chinese, new year's eve, family, review, lego 80101
layout: post
date: 2019-06-28 20:00:14 +0200
comments: true
category: blog
description:
---

This is the pictures of the set that my colleague Raúl brought back from his trip to APAC, hope you enjoy it!

There are lot of printed pieces that look very nice, even the faces have been suited for the region.

Model is [80101 Chinese New's Year Eve Dinner 🛒](https://www.amazon.es/dp/B07KRFLDLN?tag=redken-21).

<div class="elegant-gallery" itemscope itemtype="http://schema.org/ImageGallery">
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/xIIP1ER.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/xIIP1ER.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/BjgRRlC.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/BjgRRlC.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/Gmx6LsM.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/Gmx6LsM.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/zn9Y7tO.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/zn9Y7tO.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/XkDeqSU.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/XkDeqSU.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/wLlgJmj.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/wLlgJmj.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/kFHfF5s.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/kFHfF5s.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/mgpsRrT.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/mgpsRrT.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/BSoWyVV.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/BSoWyVV.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/0GDUcbW.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/0GDUcbW.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 </div>

Follow my channel on Telegram on Lego deals at <https://t.me/brickchollo>
