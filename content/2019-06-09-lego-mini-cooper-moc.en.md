---
author: Pablo Iranzo Gómez
title: Lego 75894 - 1967 Mini Cooper S Rally and Buggy MOC adaptation as Mini Transporter
tags: lego, MOC, mini, speed champions, review, lego 75894
layout: post
date: 2019-06-09 01:16:14 +0200
comments: true
category: blog
description:
---

After seeing this MOC [75894-mini-transporter](https://rebrickable.com/mocs/MOC-24636/Keep%20On%20Bricking/75894-mini-transporter/), I decided to:

- Buy the model [Lego 75894 - 1967 Mini Cooper S Rally and 2018 Mini John Cooper Works Buggy 🛒](https://www.amazon.es/dp/B07FNTMWMT?tag=redken-21)
- Buy the instructions at the MOC URL

After several times trying to pay for the instructions (which I was unable to get because
PayPal complained on the web site), I decided to make my own based on what I could see, and the result, are the images you can see, the initial model which had a small driving area and no minifig could sit there and the final one which added some more options like sizing cabin for allowing a minifigure to `drive`, plus some other alternatives for the front facing engine area and better holding of the rear wheels area.

<div class="elegant-gallery" itemscope itemtype="http://schema.org/ImageGallery">
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/RI1kBQt.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/RI1kBQt.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/wmxTdkx.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/wmxTdkx.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/FJ9KbI2.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/FJ9KbI2.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/pXGoo2k.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/pXGoo2k.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/QwC7ae6.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/QwC7ae6.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/5guNVbB.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/5guNVbB.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/HHR8GR2.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/HHR8GR2.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/2PKi2iV.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/2PKi2iV.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/9d2pTks.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/9d2pTks.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/hZUOEEi.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/hZUOEEi.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/HYGXZbE.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/HYGXZbE.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/7vv9umN.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/7vv9umN.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/gN9Hf91.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/gN9Hf91.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description"></figcaption>
    </figure>
 <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a href="https://i.imgur.com/N4Pek5y.jpg.jpg" itemprop="contentUrl" data-size="4032x3024">
            <img src="https://i.imgur.com/N4Pek5y.jpg" width="403" height="302" itemprop="thumbnail" alt="" />
        </a>
        <figcaption itemprop="caption description">The leftover pieces</figcaption>
    </figure>
 </div>

Hope you like it!

Follow my channel on Telegram on Lego deals at <https://t.me/brickchollo>
